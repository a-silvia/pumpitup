from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='driven data pump it up competition',
    author='silvia',
    license='MIT',
)
