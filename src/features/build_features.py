# -*- coding: utf-8 -*-
import click
import logging
import pandas as pd
from sklearn.externals import joblib
from pathlib import Path
from dotenv import find_dotenv, load_dotenv


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath, output_filepath):
    """ Runs data processing scripts to turn interim data from (../interim) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final feature set from interim data')
    df = pd.read_csv(input_filepath)

    logger.info(f'loaded {input_filepath}, creating new features')
    df = new_features(df, Path.cwd())

    logger.info(f'loaded {input_filepath}, selecting features')
    df = select_features(df)

    logger.info(f'one-hot-encoding categorical features')
    df = dummify(df, Path.cwd())

    logger.info(f'saving transformed data in {output_filepath}')
    df.to_csv(output_filepath, index=False)

    logger.info(f'done!')


def new_features(df, path_to_repo_root='/home/ana/Documents/DDT/pumpitup'):
    df.loc[:, 'unknown_quality'] = df.loc[:, 'quality_group'] == 'unknown'

    df.loc[:, 'date_recorded'] = pd.to_datetime(df.date_recorded)
    df['days_since_recording'] = [a.days for a in (max(df.date_recorded) - df.date_recorded)]
    scaler = joblib.load(f"{path_to_repo_root}/data/external/scaler_dayssince.save")
    df['days_since_recording'] = scaler.transform(df.days_since_recording.values.reshape(-1, 1))

    return df


def select_features(df):
    features = ['id', 'longitude_imp', 'latitude_imp', 'amount_tsh', 'basin',
                'unknown_quality', 'extraction_type_class', 'quantity', 'source',
                'waterpoint_type', 'management', 'payment', 'population', 'population_0',
                'days_since_recording', 'construction_year', 'construction_year_missing']
    df = df[features]

    return df


def dummify(df, path_to_repo_root='/home/ana/Documents/DDT/pumpitup'):
    dummies = ['basin', 'extraction_type_class', 'quantity', 'source',
               'waterpoint_type', 'management', 'payment']

    df = pd.get_dummies(df, columns=dummies)

    expected = ['id', 'longitude_imp', 'latitude_imp', 'amount_tsh', 'unknown_quality',
                'population', 'population_0', 'days_since_recording', 'basin_Internal',
                'basin_Lake Nyasa', 'basin_Lake Rukwa', 'basin_Lake Tanganyika',
                'basin_Lake Victoria', 'basin_Pangani', 'basin_Rufiji',
                'basin_Ruvuma / Southern Coast', 'basin_Wami / Ruvu',
                'extraction_type_class_gravity', 'extraction_type_class_handpump',
                'extraction_type_class_motorpump', 'extraction_type_class_other',
                'extraction_type_class_rope pump', 'extraction_type_class_submersible',
                'extraction_type_class_wind-powered', 'quantity_dry', 'quantity_enough',
                'quantity_insufficient', 'quantity_seasonal', 'quantity_unknown',
                'source_dam', 'source_hand dtw', 'source_lake', 'source_machine dbh',
                'source_other', 'source_rainwater harvesting', 'source_river',
                'source_shallow well', 'source_spring', 'source_unknown',
                'waterpoint_type_cattle trough', 'waterpoint_type_communal standpipe',
                'waterpoint_type_communal standpipe multiple', 'waterpoint_type_dam',
                'waterpoint_type_hand pump', 'waterpoint_type_improved spring',
                'waterpoint_type_other', 'management_company', 'management_other',
                'management_parastatal', 'management_private operator',
                'management_school', 'management_trust', 'management_vwc',
                'management_water authority', 'management_water board',
                'management_wua', 'management_wug', 'payment_never pay',
                'payment_other', 'payment_pay annually', 'payment_pay monthly',
                'payment_pay per bucket', 'payment_pay when scheme fails',
                'payment_unknown', 'construction_year', 'construction_year_missing']

    for col in set(expected).difference(set(df.columns)):
        print(f"{col} was missing in the original data, filling with 0s")
        df.loc[:, col] = [0] * len(df)

    for col in set(df.columns).difference(set(expected)):
        print(f"popping {col} from final dataset")
        df.pop(col)

    return df


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
