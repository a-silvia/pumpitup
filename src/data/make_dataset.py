# -*- coding: utf-8 -*-
import click
import logging
import pandas as pd
import numpy as np
from sklearn.externals import joblib
from pathlib import Path
from dotenv import find_dotenv, load_dotenv


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath, output_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')
    df = pd.read_csv(input_filepath)

    logger.info(f'loaded {input_filepath}, applying base_transformations')
    df = base_transformations(df, Path.cwd())

    logger.info(f'saving transformed data in {output_filepath}')
    df.to_csv(output_filepath, index=False)

    logger.info(f'done!')


def base_transformations(df, path_to_repo_root='/home/ana/Documents/DDT/pumpitup'):
    """
    transformations defined in the notebook silvia/01-clean_redundant_features.ipynb.
    takes the raw dataframe downloaded from https://www.drivendata.org/competitions/7/pump-it-up-data-mining-the-water-table/
    and returns a transformed version of it
    """
    
    # Missing coordinates: replace 0s in coordinates by the median coordinates of the well's region
    # the median coordinates for each region are saved in data/external/imput_coords.csv
    coord_imput = pd.read_csv(f"{path_to_repo_root}/data/external/imput_coords.csv")
    df.loc[:, 'longitude_imp'] = df['longitude']
    df.loc[:, 'latitude_imp'] = df['latitude']
    
    for r in df.loc[np.isclose(df.longitude, 0), 'region_code'].unique():

        n = 0
        lat = 0
        lon = 0

        try:
            lat = coord_imput.loc[r, 'latitude']
            lon = coord_imput.loc[r, 'longitude']
            n = len(df.loc[(np.isclose(df.longitude, 0)) & (df.region_code == r), :])

            df.loc[(np.isclose(df.longitude, 0)) & (df.region_code == r), 'longitude_imp'] = [lon] * n
            df.loc[(np.isclose(df.longitude, 0)) & (df.region_code == r), 'latitude_imp'] = [lat] * n

        except KeyError:
            print(f'no imput value for {r}')
            continue
    
    # Missing population: no actual missing values but a large amount of 0s which may or may not be real.
    # add a boolean variable that denotes if population is 0 or not
    df['population_0'] = df.population == 0
    df['construction_year_missing'] = df['construction_year'] < 1000
    
    # Remove redundant features (check notebook for reasoning)
    features = ['id', 'longitude_imp', 'latitude_imp', 'amount_tsh', 'date_recorded', 'funder', 
                'gps_height', 'basin', 'scheme_management', 'extraction_type_class', 'management', 
                'population', 'population_0', 'public_meeting', 'payment', 'quality_group', 
                'quantity', 'source', 'waterpoint_type', 'construction_year', 'construction_year_missing']
    df = df.loc[:, features]
    
    # `management` includes values which are redundant; replace these
    df.loc[:, 'management'] = df['management'].replace('unknown', 'other')
    df.loc[:, 'management'] = df['management'].replace('other - school', 'school')
    
    # fill missing values
    df.loc[:, 'scheme_management'] = df.scheme_management.fillna('Other')
    df.loc[:, 'public_meeting'] = df.public_meeting.fillna(True)
    df.loc[:, 'funder'] = df.funder.fillna('unknown')
    df.loc[df['construction_year'] < 1000, 'construction_year'] = [2000] * len(df[df['construction_year'] < 1000])
    
    # Scale the data. The scalers were trained on a fraction of the training set
    scaler = joblib.load(f"{path_to_repo_root}/data/external/scaler_minmax.save") 
    to_scale = ['latitude_imp', 'longitude_imp', 'gps_height', 'construction_year']
    df.loc[:, to_scale] = scaler.transform(df[to_scale])
    
    scaler = joblib.load(f"{path_to_repo_root}/data/external/scaler_standard.save") 
    to_scale = ['population', 'amount_tsh']
    df.loc[:, to_scale] = scaler.transform(df[to_scale])
    
    # the final dataset is expected to have the following columns:
    expected = ['id', 'longitude_imp', 'latitude_imp', 'amount_tsh', 'date_recorded',
                'funder', 'gps_height', 'basin', 'scheme_management', 'extraction_type_class', 
                'management', 'population', 'population_0', 'public_meeting', 'payment', 
                'quality_group', 'quantity', 'source', 'waterpoint_type', 'construction_year', 
                'construction_year_missing']
    

    # the following cycles are meant to confirm that the resulting dataframe contains the correct columns
    for col in set(expected).difference(set(df.columns)):
        print(f"{col} was missing in the original data, filling with 0s")
        df.loc[:, col] = [0] * len(df)
        
    for col in set(df.columns).difference(set(expected)):
        print(f"popping {col} from final dataset")
        df.pop(col)
    
    return df


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
