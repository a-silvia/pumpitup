pumpitup
==============================

driven data pump it up competition

Get started
------------

- download the files from the [pump it up competition](https://www.drivendata.org/competitions/7/pump-it-up-data-mining-the-water-table/) into a `data` folder
- run the `make_dataset.py` code to do some preliminary transformations to the data (remove redundant features, scale numerical features):
``` python
python .\src\data\make_dataset.py .\data\raw\4910797b-ee55-40a7-8668-10efd5c1b960.csv .\data\interim\train.csv
python .\src\data\make_dataset.py .\data\raw\702ddfc5-68cd-4d1d-a0de-f5f566f76d91.csv .\data\interim\test.csv 
```
- run the `build_features.py` code to select some features, create new ones, one-hot-encode categorical features.
``` python
python .\src\features\build_features.py .\data\interim\train.csv .\data\processed\train.csv
python .\src\features\build_features.py .\data\interim\test.csv .\data\processed\test.csv
```
- the data is now ready to train models on!

Notebook Reference
------------

- 01 is where I looked into initial feature cleaning
- 02 is where I add features and do some selection
- 03 is where I train a preliminary model to get a baseline (leaderboard score was 0.8022)
- 04 is where I train some XGB models (highest score so far was 0.8143)


Cookiecutter Data Science: Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   └── visualization  <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.testrun.org


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
